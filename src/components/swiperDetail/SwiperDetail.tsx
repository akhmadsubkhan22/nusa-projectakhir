import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'

// Import Swiper styles
import 'swiper/css'
import 'swiper/css/free-mode'
import 'swiper/css/pagination'

// import required modules
import { FreeMode, Navigation, Pagination } from 'swiper/modules'
import ButtonSwiper from 'components/Button/ButtonSwiper'

interface ISwiper {
  sebelumText?: string
  berikutnyaText?: string
  swiperStyle?: string
}

function Slider(props: ISwiper) {
  const { swiperStyle, sebelumText, berikutnyaText} = props
  return (
    <div className="flex">
      <Swiper
        slidesPerView="auto"
        spaceBetween={30}
        freeMode
        pagination={{
          dynamicBullets: true,
        }}
        modules={[FreeMode, Navigation, Pagination]}
        className={`${swiperStyle} relative`}
      >
        <SwiperSlide className="w-full flex">
          <ButtonSwiper.ButtonSlidePrev
            className="border-none w-[80px] h-[80px] shadow-none"
            icon="./static/images/icons/chevron-left.svg"
          />
          <div className="flex justify-between w-[1450px]">
            <div>
              <p className="text-[#404258] text-[16px]">Sebelumnya</p>
              <h1 className="text-[28px] text-[#404258]">
                {sebelumText} 
              </h1>
            </div>
            <div>
              <p className="text-[#404258] text-[16px]">Berikutnya</p>
              <h1 className="text-[28px] text-[#404258]">{berikutnyaText}</h1>
            </div>
          </div>
          <ButtonSwiper.ButtonSlideNext
            className="border-none w-[80px] h-[80px] shadow-none"
            icon="./static/images/icons/chevron-right.svg"
          />
        </SwiperSlide>
        <SwiperSlide className="w-full flex ">
          <ButtonSwiper.ButtonSlidePrev
            className="border-none w-[80px] h-[80px] shadow-none"
            icon="./static/images/icons/chevron-left.svg"
          />
          <div className="flex justify-between w-[1450px]">
            <div>
              <p className="text-[#404258] text-[16px]">Sebelumnya</p>
              <h1 className="text-[28px] text-[#404258]">{sebelumText}</h1>
            </div>
            <div>
              <p className="text-[#404258] text-[16px]">Berikutnya</p>
              <h1 className="text-[28px] text-[#404258]">{berikutnyaText}</h1>
            </div>
          </div>
          <ButtonSwiper.ButtonSlideNext
            className="border-none w-[80px] h-[80px] shadow-none"
            icon="./static/images/icons/chevron-right.svg"
          />
        </SwiperSlide>
      </Swiper>
    </div>
  )
}

export default Slider
