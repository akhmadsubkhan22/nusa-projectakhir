import React, { useState } from 'react'
import { Typography } from 'antd'
import { newsData } from 'data/newsData'
import { eventData } from 'data/eventData'
import CardNews from 'components/Cards/CardNews'
import BreadCrumb from 'components/BreadCrumbs/BreadCrumbs'
import PaginationComp from 'components/Pagination/Pagination'
import TopicFilter from 'components/TopicFilter/TopicFilter'

const itemsPerPage = 8

const breadcrumbItems = [{ text: 'Blog' }]
interface CardSmallProps {
  image: string
  title: string
  kategori: string
  date: string
}

function CardSmall({ image, title, kategori, date }: CardSmallProps) {
  return (
    <div className="w-[28rem] flex items-center justify-start my-9">
      <div className="w-[6rem] h-[100px] my-auto ">
        <img
          src={image}
          className="object-cover w-[90px] h-[90px] border-rad mr-4"
          alt="CardSmall"
          style={{ borderRadius: '8px' }}
        />
      </div>
      <div>
        <Typography.Title
          level={3}
          className="text-[16px] text-[#404258] ml-3 font-semibold"
        >
          {title}
          {/* text-[42px]  font-semibold */}
        </Typography.Title>
        <Typography.Text type="secondary" className="text-xs mt-1 ms-4">
          <span className="text-red-600 font-extrabold">{kategori}</span>
          <span className="text-gray-500 font-normal ml-2">❁ {date}</span>
        </Typography.Text>
      </div>
    </div>
  )
}

function Blog() {
  const [currentPage, setCurrentPage] = useState(1)
  const totalPages = Math.ceil(newsData.length / itemsPerPage)

  const startIndex = (currentPage - 1) * itemsPerPage
  const endIndex = startIndex + itemsPerPage

  const dataNews = newsData.slice(startIndex, endIndex)

  const goToPage = (page) => {
    setCurrentPage(page)
  }

  return (
    /* ... Hero Page ... */
    <div className="container mx-auto">
      <div className="w-full h-[43rem] bg-white">
        <div className="w-full pr-6 flex justify-center items-center">
          <div className="w-[60rem] pr-[40px] mt-6">
            <BreadCrumb items={breadcrumbItems} />
            <img
              src="./static/images/content/blog/coding-screen.jpeg"
              className="object-cover w-[70rem] h-[33rem] border-rad"
              alt="konten"
              style={{ borderRadius: '15px' }}
            />
            <Typography.Title
              level={3}
              className="text-[37px] 
              text-[#404258]
              font-semibold
              mt-3"
            >
              10 Ekstensi terbaik untuk Visual Studio Code
            </Typography.Title>
            <Typography.Text type="secondary" className="text-xs">
              <span className="text-red-600 font-extrabold mr-2">
                Engineering
              </span>{' '}
              ❁
              <span className="text-gray-500 font-normal ml-2">
                18 Mei 2020
              </span>
            </Typography.Text>
          </div>
          <div className="w-1/3">
            <div className="w-1/3">
              <CardSmall
                image="./static/images/content/blog/ngoding.jpeg"
                title="Remote Work Tools"
                kategori="Tips"
                date="30 februari 2023"
              />
              <CardSmall
                image="./static/images/content/blog/coding-screen.jpeg"
                title="Text Editor Terbaik Untuk Ngoding di Tahun 2020"
                kategori="Engineering"
                date="14 Juli 2023"
              />
              <CardSmall
                image="./static/images/content/blog/gitlab.jpeg"
                title="Mengelola Project Dengan Lebih Mudah Dengan GIT"
                kategori="Project Management"
                date="8 Juni 2023"
              />
              <CardSmall
                image="./static/images/content/blog/nodejs.png"
                title="Mengenal REST API NodeJS #1"
                kategori="Engineering"
                date="18 Juni 2023"
              />
            </div>
          </div>
        </div>
      </div>
      {/* End */}

      {/* ... Popular Article ... */}
      <div className="flex justify-between items-center">
        <div>
          <div className="flex gap-3 mt-[70px]">
            <h1 className="text-[42px] text-[#404258] font-semibold">
              Most Popular Article
            </h1>
          </div>
        </div>
      </div>
      <div className="flex gap-[24px] justify-center mt-2">
        {newsData.map((e) => (
          <CardNews
            size="w-[411px]"
            image={e.img}
            title={e.title}
            category={e.category}
            date={e.date}
          />
        ))}
      </div>
      {/* end */}

      {/* Browse by topic  */}
      <div className="flex justify-between items-center mt-[85px]">
        <div>
          <div className="flex gap-3">
            <h1 className="text-[42px] text-[#404258] font-semibold">
              Browse by topic
            </h1>
          </div>
        </div>
      </div>
      <TopicFilter />
      <div className="flex gap-[24px] justify-center mt-7">
        {newsData.map((e) => (
          <CardNews
            size="w-[411px]"
            image={e.img}
            title={e.title}
            category={e.category}
            date={e.date}
          />
        ))}
      </div>
      <div className="flex gap-[24px] justify-center mt-7">
        <div className="flex  gap-[24px]">
          {dataNews.map((e, index) => (
            <div key={index} className="w-[411px] mb-10 mr-4 md:mr-0">
              <CardNews
                size="w-[411px]"
                image={e.img}
                title={e.title}
                category={e.category}
                date={e.date}
              />
            </div>
          ))}
        </div>
      </div>
      <div className="flex gap-[24px] justify-center mt-7">
        {dataNews.map((e) => (
          <CardNews
            size="w-[411px]"
            image={e.img}
            title={e.title}
            category={e.category}
            date={e.date}
          />
        ))}
      </div>
      <div className="mb-[129px] mt-[60px]">
        <PaginationComp
          currentPage={currentPage}
          totalPages={totalPages}
          onPageChange={goToPage}
          itemsPerPage={itemsPerPage}
        />
      </div>
    </div>
  )
}

export default Blog
